﻿using bot.core.Abstractions;
using bot.core.Services;
using bot.core.Structures;
using Telegram.Bot;
using Telegram.Bot.Polling;
using Telegram.Bot.Types;
namespace telegram_bot.app
{

    class TelegramBot
    {
        private readonly static ITelegramBotClient bot = new TelegramBotClient("5749548216:AAHV486SxHZFL0via3fi5W5qxmFyZIvLdxQ");
        private readonly static ILoggingService logger = new LoggingService();
        private static readonly BiDictionary<long> _connections = new();
        private static readonly Dictionary<long, bool> _waitingMode = new();
        public static async Task HandleUpdateAsync(ITelegramBotClient botClient, Update update, CancellationToken cancellationToken)
        {
            await logger.LogInformation(Newtonsoft.Json.JsonConvert.SerializeObject(update, Newtonsoft.Json.Formatting.Indented));
            if (update.Type == Telegram.Bot.Types.Enums.UpdateType.Message)
            {
                var message = update.Message;
                if (_waitingMode.ContainsKey(message!.Chat.Id) && _waitingMode[message!.Chat.Id])
                {
                    if (message.Text!.StartsWith("c - "))
                    {
                        var splittedMsg = message!.Text.Split(' ');
                        if (!long.TryParse(splittedMsg[^1], out long id))
                        {
                            await botClient.SendTextMessageAsync(message.Chat, "Неправильный уникальный номер, попробуйте заново!");
                            return;
                        }
                        _connections.Add(message.Chat.Id, id);
                        await botClient.SendTextMessageAsync(message.Chat, $"Вы успешно подключились к {botClient.GetChatAsync(_connections[message.Chat.Id]).Result.FirstName}");
                        await botClient.SendTextMessageAsync(id, $"{message.Chat.FirstName} установил с вами соединение");
                        _waitingMode.Remove(message.Chat.Id);
                        return;
                    }
                }

                if (message.Text!.ToLower() == "/start")
                {
                    await botClient.SendTextMessageAsync(message.Chat, "Добро пожаловать, добрый путник!");
                    return;
                }

                if (message.Text!.ToLower() == "/getid")
                {
                    await botClient.SendTextMessageAsync(message.Chat, message.Chat.Id.ToString());
                    return;
                }

                if (message.Text.ToLower() == "/connect")
                {
                    if (_connections.ContainsKey(message.Chat.Id))
                    {
                        await botClient.SendTextMessageAsync(message.Chat, "У вас уже есть существующее подключение, разорвите его преждем чем сопрягаться с другим");
                        return;
                    }
                    await botClient.SendTextMessageAsync(message.Chat, "Отправьте уникальный номер товарища в чат, в формате:\nc - id\n\nОтправьте /cancel для того чтобы отменить операцию.");
                    _waitingMode.Add(message.Chat.Id, true);
                    return;
                }

                if (message.Text!.ToLower() == "/break")
                {
                    if (!_connections.ContainsKey(message.Chat.Id))
                    {
                        await botClient.SendTextMessageAsync(message.Chat, "У вас нет сопряжений с кем либо, чтобы их разрывать");
                        return;
                    }
                    var targetId = _connections[message.Chat.Id];
                    await botClient.SendTextMessageAsync(message.Chat, $"Вы успешно разорвали подключение с {botClient.GetChatAsync(targetId).Result.FirstName}");
                    await botClient.SendTextMessageAsync(targetId, $"{message.Chat.FirstName} разорвал с вами подключение");
                    _connections.Remove(message.Chat.Id);
                }

                if (message.Text!.ToLower() == "/kiss" && _connections.ContainsKey(message.Chat.Id))
                {
                    var random = new Random();
                    var randNumber = random.Next(1, 101);
                    string numberDeclination = $"{randNumber} {((randNumber % 10 == 1 && randNumber % 100 != 11) ? "раз" : (randNumber % 10 >= 2 && randNumber % 10 <= 4 && (randNumber % 100 < 10 || randNumber % 100 >= 20) ? "раза" : "раз"))}";

                    await botClient.SendTextMessageAsync(message.Chat, $"Вы поцеловали {botClient.GetChatAsync(_connections[message.Chat.Id]).Result.FirstName} x{numberDeclination}");
                    await botClient.SendTextMessageAsync(_connections[message.Chat.Id], $"{message.Chat.FirstName} поцеловал вас x{numberDeclination}");
                    return;
                }

                if (message.Text!.ToLower() == "/cancel" && _waitingMode[message.Chat.Id])
                {
                    _waitingMode.Remove(message.Chat.Id);
                    return;
                }
            }
        }

        public static async Task HandleErrorAsync(ITelegramBotClient botClient, Exception exception, CancellationToken cancellationToken)
        {
            await logger.LogError(Newtonsoft.Json.JsonConvert.SerializeObject(exception, Newtonsoft.Json.Formatting.Indented));
        }


        static void Main(string[] args)
        {
            Console.WriteLine("Bot launched " + bot.GetMeAsync().Result.FirstName);

            var cts = new CancellationTokenSource();
            var cancellationToken = cts.Token;
            var receiverOptions = new ReceiverOptions
            {
                AllowedUpdates = { }, 
            };
            bot.StartReceiving(
                HandleUpdateAsync,
                HandleErrorAsync,
                receiverOptions,
                cancellationToken
            );
            Console.ReadLine();
        }
    }
}