﻿namespace bot.core.Structures
{
    public class BiDictionary<TKeyValue> where TKeyValue : notnull
    {
        private readonly Dictionary<TKeyValue, TKeyValue> _dictionary;
        private readonly Dictionary<TKeyValue, TKeyValue> _reverseDictionary;

        public BiDictionary()
        {
            _dictionary = new Dictionary<TKeyValue, TKeyValue>();
            _reverseDictionary = new Dictionary<TKeyValue, TKeyValue>();
        }

        public TKeyValue this[TKeyValue index]
        {
            get 
            {
                if (_dictionary.ContainsKey(index)) 
                {
                    return _dictionary[index];
                }
                return _reverseDictionary[index];
            }
            set 
            { 
                if (_dictionary.ContainsKey(index))
                {
                    _dictionary[index] = value;
                    return;
                }
                _reverseDictionary[index] = value;
            }
        }

        public void Add(TKeyValue key, TKeyValue value)
        {
            _dictionary.Add(key, value);
            _reverseDictionary.Add(value, key);
        }

        public void Remove(TKeyValue key)
        {
            if (_dictionary.ContainsKey(key))
            {
                TKeyValue value = _dictionary[key];
                _dictionary.Remove(key);
                _reverseDictionary.Remove(value);
            }
            else if (_reverseDictionary.ContainsKey(key))
            {
                TKeyValue value = _reverseDictionary[key];
                _reverseDictionary.Remove(key);
                _dictionary.Remove(value);
            }
        }

        public bool TryGetValueByKey(TKeyValue key, out TKeyValue value)
        {
            return _dictionary.TryGetValue(key, out value);
        }

        public bool TryGetValueByValue(TKeyValue value, out TKeyValue key)
        {
            return _reverseDictionary.TryGetValue(value, out key);
        }

        public bool ContainsKey(TKeyValue keyValue)
        {
            return _dictionary.ContainsKey(keyValue) || _reverseDictionary.ContainsKey(keyValue);
        }
    }
}
