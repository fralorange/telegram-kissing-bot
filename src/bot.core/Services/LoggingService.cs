﻿using bot.core.Abstractions;

namespace bot.core.Services
{
    public class LoggingService : ILoggingService
    {
        public Task LogError(string message)
        {
            Console.WriteLine($"Error: {message}");
            return Task.CompletedTask;
        }

        public Task LogInformation(string message)
        {
            Console.WriteLine($"{message}");
            return Task.CompletedTask;
        }
    }
}
