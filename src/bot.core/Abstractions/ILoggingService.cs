﻿namespace bot.core.Abstractions
{
    public interface ILoggingService
    {
        Task LogInformation(string message);
        Task LogError(string message);

    }
}
